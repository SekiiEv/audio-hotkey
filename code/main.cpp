#include <objbase.h>
#include <mmdeviceapi.h>
#include <audiopolicy.h>

#include <Windows.h>

#include <stdio.h>
#include <vector>

// MSDN useful macros
#define EXIT_ON_ERROR(hr);  \
if (FAILED(hr)) { goto Exit; }
#define SAFE_RELEASE(punk);  \
if ((punk) != NULL)  \
{ (punk)->Release(); (punk) = NULL; }

struct AudioSession
{
    GUID groupGuid;
    std::vector<ISimpleAudioVolume*> volumeList;
    PWSTR name;
    
    void SetVolume(float volume)
    {
        for (ISimpleAudioVolume* v : volumeList)
        {
            if (FAILED(v->SetMasterVolume(volume, NULL)))
            {
                printf("Could not set master volume of one element\n");
                break;
            }
        }
    }
    
    void SetMute(bool mute)
    {
        for (ISimpleAudioVolume* v : volumeList)
        {
            if (FAILED(v->SetMute(mute, NULL)))
            {
                printf("Could not mute the volume of one element\n");
                break;
            }
        }
    }
    
};

static std::vector<AudioSession> audioSessionList;
static IAudioSessionEnumerator *SessionEnumerator = NULL;

int main(int argc, char* argv[])
{
    HRESULT hr = S_OK;
    IMMDeviceEnumerator   *DeviceEnumerator = NULL;
    IMMDeviceCollection   *DeviceCollection = NULL;
    IMMDevice             *DeviceEndpoint   = NULL;
    IAudioSessionManager2 *SessionManager   = NULL;
    
    // Initialize SessionEnumerator
    
    hr = CoInitialize(NULL);
    EXIT_ON_ERROR(hr);
    
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_ALL,__uuidof(IMMDeviceEnumerator), (void**)&DeviceEnumerator);
    EXIT_ON_ERROR(hr);
    
    hr = DeviceEnumerator->EnumAudioEndpoints(eRender, DEVICE_STATE_ACTIVE,&DeviceCollection);
    EXIT_ON_ERROR(hr);
    
    hr = DeviceCollection->Item(0, &DeviceEndpoint);
    EXIT_ON_ERROR(hr);
    
    hr = DeviceEndpoint->Activate(__uuidof(IAudioSessionManager2), CLSCTX_ALL, NULL, (void**) &SessionManager);
    EXIT_ON_ERROR(hr);
    
    hr = SessionManager->GetSessionEnumerator(&SessionEnumerator);
    EXIT_ON_ERROR(hr);
    
    SAFE_RELEASE(DeviceEnumerator);
    SAFE_RELEASE(DeviceCollection);
    SAFE_RELEASE(DeviceEndpoint);
    SAFE_RELEASE(SessionManager);
    CoUninitialize();
    
    // Print all active sessions' name and group id
    
    int SessionCount;
    hr = SessionEnumerator->GetCount(&SessionCount);
    EXIT_ON_ERROR(hr);
    //printf("Session Count = %d\n", SessionCount);
    
    IAudioSessionControl *SessionControl = NULL;
    ISimpleAudioVolume *SimpleAudioVolume = NULL;
    for (int i = 0; i < SessionCount; ++i)
    {
        hr = SessionEnumerator->GetSession(i, &SessionControl);
        EXIT_ON_ERROR(hr);
        
        PWSTR sessionName;
        hr = SessionControl->GetDisplayName(&sessionName);
        EXIT_ON_ERROR(hr);
        
        GUID groupGuid;
        hr = SessionControl->GetGroupingParam(&groupGuid);
        EXIT_ON_ERROR(hr);
        
        SessionControl->QueryInterface(__uuidof(ISimpleAudioVolume), (void**) &SimpleAudioVolume);
        float volume;
        SimpleAudioVolume->GetMasterVolume(&volume);
        EXIT_ON_ERROR(hr);
        
        bool sessionFound = false;
        for (AudioSession& as : audioSessionList)
        {
            if (as.groupGuid == groupGuid)
            {
                as.volumeList.push_back(SimpleAudioVolume);
                sessionFound = true;
                break;
            }
        }
        
        if (!sessionFound)
        {
            AudioSession as;
            
            as.groupGuid = groupGuid;
            as.name = sessionName;
            as.volumeList.push_back(SimpleAudioVolume);
            audioSessionList.push_back(as);
            
            printf("F%zd - %8x: %ls\n", audioSessionList.size(), as.groupGuid.Data1, as.name);
        }
        
        SAFE_RELEASE(SessionControl);
        CoTaskMemFree(sessionName);
    }
    
    // Check key inputs
    
    bool escPressed = (GetKeyState(VK_ESCAPE) & 0x8000);
    while(!escPressed)
    {
        for (int i = 0; i < audioSessionList.size(); ++i)
        {
            bool fPressed = (GetKeyState(VK_F1 + i) & 0x8000);
            if (fPressed)
            {
                bool shiftPressed   = (GetKeyState(VK_SHIFT)   & 0x8000);
                bool controlPressed = (GetKeyState(VK_CONTROL) & 0x8000);
                
                if (shiftPressed)
                {
                    audioSessionList[i].SetMute(false);
                }
                else if (controlPressed)
                {
                    audioSessionList[i].SetMute(true);
                }
            }
        }
        escPressed = (GetKeyState(VK_ESCAPE) & 0x8000);
    }
    
    return 0;
    
    Exit:
    SAFE_RELEASE(DeviceEnumerator);
    SAFE_RELEASE(DeviceCollection);
    SAFE_RELEASE(DeviceEndpoint);
    SAFE_RELEASE(SessionManager);
    CoUninitialize();
    SAFE_RELEASE(SessionEnumerator);
    SAFE_RELEASE(SessionControl);
    SAFE_RELEASE(SimpleAudioVolume);
    return -1;
}
