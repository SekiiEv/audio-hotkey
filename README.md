# Audio Hotkey

Simple C++ code to mute applications using the function keys (F-keys).

The program shows the available applications to mute/unmute. The key bindings are as follow:
* CONTROL + Fn: Mute
* SHIFT + Fn: Unmute

![](git-img/main.png)